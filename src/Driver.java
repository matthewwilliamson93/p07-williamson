import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Vector;
import java.util.Collections;

/**
 * Current test driver for the Ghost game
 * TODO: Refactor for Android
 *
 * @author Matthew Williamson
 */
public class Driver {
	public static void main(String args[]) {
		// Load Dictionary
		Trie dict = new Trie();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("dict.txt")));
			String word;
			while ((word = br.readLine()) != null) {
				dict.insert(word);
			}
			br.close();
		}
		catch (Exception e) {
			System.err.println("File not found exception.");
		}

		// Choose players
		Vector<Player> players = new Vector<>();
		int inc = 1;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			while(true) {
				System.out.print("Would you like to add human, computer, or no more players? (h/c/Anything else) ");
				String opt = br.readLine();
				if (opt.equalsIgnoreCase("h")) {
					System.out.print("\nWhat should their name be? ");
					String name = br.readLine();
					players.add(new HumanPlayer(name));
				}
				else if (opt.equalsIgnoreCase("c")) {
					System.out.print("\nWhat should their difficulty be? (1/2/3) ");
					int diff = Integer.parseInt(br.readLine());
					players.add(new ComputerPlayer(inc, diff, dict));
					inc++;
				}
				else {
					break;
				}
				System.out.println();
			}
		}
		catch (Exception e) {
			System.err.println("Can't read from stdin");
		}
		Collections.shuffle(players);

		System.out.println("Let's begin!");
		// Run the game
		Game g = new Game(dict, players);
		Player winner = g.play();

		// Handle winner
		System.out.println("The winner is, " + winner.name());
	}
}

