# README #

Ghost

Core java implementation only. This was imported into the p07-weachock repo.

Ghost is a game where players have to say a letter to extend to the existing
string without completeing the word. If you do then you lose. The winner is
the last player standing.

Name: Matthew Williamson
Email: mwilli20@binghamton.edu

Name: John Weachock
Email: jweacho1@binghamton.edu