import java.util.Vector;
import java.util.Random;

/**
 * Game class simulates a game of Ghost
 *
 * @author Matthew Williamson
 */
public class Game {
	private Trie dict;
	private Vector<Player> players;
	private Random rand;
	private int cur;
	private String word;

	/**
	 * Constructor for game
	 *
	 * @param dict    The dictionary to be playing with
	 * @param players The players in the game
	 */
	public Game(Trie dict, Vector<Player> players) {
		this.dict = dict;
		this.players = new Vector<>(players);
		this.rand = new Random();
		this.cur = rand.nextInt(this.players.size());
		this.word = "";
	}

	// TODO: Refactor for Android
	/**
	 * Play simulates the game
	 *
	 * @return The winner of the game
	 */
	public Player play() {
		while (true) {
			Player currentPlayer = this.players.get(this.cur);
			this.word = this.word + currentPlayer.play(this.word);
			if (this.dict.contains(this.word)) {
				System.out.println(this.word + " is a word.");
			}
			else if (!this.dict.isPrefix(this.word)) {
				System.out.println("No word starts with " + this.word + ".");
			}
			else {
				this.cur++;
				this.cur = this.cur % this.players.size();
				continue;
			}
			System.out.println(currentPlayer.name() + " is removed.\n");
			this.players.remove(this.cur);
			this.cur = rand.nextInt(this.players.size());
			this.word = "";
			if (this.players.size() == 1) {
				return this.players.get(0);
			}
		}
	}
}

